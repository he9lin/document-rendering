namespace DocumentRendering
open System.Drawing

type Rect =
  { Left : float32
    Top : float32 
    Width : float32 
    Height : float32 }

type TextContent = 
  { Text : string
    Font : Font }

type ScreenElement =
  | TextElement of TextContent * Rect
  | ImageElement of string * Rect

type Orientation =
| Vertical
| Horizontal

type DocumentPart =
| SplitPart of Orientation * list<DocumentPart>
| TitledPart of TextContent * DocumentPart
| TextPart of TextContent
| ImagePart of string