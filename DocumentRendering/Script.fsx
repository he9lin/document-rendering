#load "DocumentRendering.fs"
open System.Web.UI.WebControls
open DocumentRendering
open System.Drawing

// Define your library scripting code here
let rc = { Left = 10.0f; Top = 10.0f; Width = 100.0f; Height = 200.0f; }
let rc2 = { rc with Left = rc.Left + 100.0f }
let deflate(original, wspace, hspace) =
  { Left = original.Left + wspace
    Top = original.Top + hspace
    Width = original.Width - (2.0f * wspace)
    Height = original.Height - (2.0f * hspace) }

let toRectangleF(original) =
  RectangleF(original.Left, original.Top, original.Width, original.Height)

let fntText = new Font("Calibri", 12.0f)
let fntHead = new Font("Calibri", 15.0f)
let elements =
  [ TextElement
      ({ Text = "Functional Programming for the Real World"
         Font = fntHead },
       { Left = 10.0f; Top = 0.0f; Width = 410.0f; Height = 30.0f });
    ImageElement
      ("DocumentRendering\cover.jpg",
       { Left = 120.0f; Top = 30.0f; Width = 150.0f; Height = 200.0f });
    TextElement
      ({ Text = "In this book, we'll introduce you to the essential " +
                "concepts of functional programming, but thanks to the .NET " +
                "Framework, we won't be limited to theoretical examples. " +
                "We'll use many of the rich .NET libraries to show how " +
                "functional programming can be used in the real world."
         Font = fntText },
       { Left = 10.0f; Top = 230.0f; Width = 400.0f; Height = 400.0f }) ]

let drawElements elements (gr:Graphics) =
  for p in elements do
    match p with
    | TextElement(text, boundingBox) ->
        let boxf = toRectangleF(boundingBox)
        gr.DrawString(text.Text, text.Font, Brushes.Black, boxf)
    | ImageElement(imagePath, boundingBox) ->
        let bmp = new Bitmap(imagePath)
        let wspace, hspace =
          boundingBox.Width / 10.0f, boundingBox.Height / 10.0f
        let rc = toRectangleF(deflate(boundingBox, wspace, hspace))
        gr.DrawImage(bmp, rc)

let drawImage (width:int, height:int) space coreDrawingFunc =
  let bmp = new Bitmap(width, height)
  use gr = Graphics.FromImage(bmp)
  gr.Clear(Color.White)
  gr.TranslateTransform(space, space)
  coreDrawingFunc(gr)
  bmp

let docImage = drawImage (450, 400) 20.0f (drawElements elements)

// let doc =
//   TitledPart(
//     { Text = "Functional Programming for the Real World";
//       Font = fntHead },
//     SplitPart(
//       Vertical,
//       [ ImagePart("DocumentRendering\cover.jpg");
//         TextPart({ Text = "..."; Font = fntText })]))

let rec documentToScreen(doc, bounds) =
  match doc with
  | SplitPart(Horizontal, parts) ->
    let width = bounds.Width / (float32(parts.Length))
    parts
    |> List.mapi (fun i part ->
        let left = bounds.Left + float32(i) * width
        let bounds = { bounds with Left = left; Width = width }
        documentToScreen(part, bounds))
    |> List.concat

  | SplitPart(Vertical, parts) ->
    let height = bounds.Height / float32(parts.Length)
    parts
    |> List.mapi (fun i part ->
      let top = bounds.Top + float32(i) * height
      let bounds = { bounds with Top = top; Height = height }
      documentToScreen(part, bounds))
    |> List.concat

  | TitledPart(tx, content) ->
    let titleBounds = { bounds with Height = 35.0f }
    let restBounds = { bounds with Height = bounds.Height - 35.0f; Top = bounds.Top + 35.0f }
    let convertedBody = documentToScreen(content, restBounds)
    TextElement(tx, titleBounds)::convertedBody

  | TextPart(tx) -> [ TextElement(tx, bounds) ]
  | ImagePart(im) -> [ ImageElement(im, bounds) ]

#r "System.Core.dll"
#r "System.Xml.Linq.dll"
open System.Xml.Linq
let attr(node:XElement, name, defaultValue) =
  let attr = node.Attribute(XName.Get(name))
  if (attr <> null) then attr.Value else defaultValue
let parseOrientation(node) =
  match attr(node, "orientation", "") with
  | "horizontal" -> Horizontal
  | "vertical" -> Vertical
  | _ -> failwith "Unknown orientation!"

let parseFont(node) =
  let style = attr(node, "style", "")
  let style =
    match style.Contains("bold"), style.Contains("italic") with
    | true, false -> FontStyle.Bold
    | false, true -> FontStyle.Italic
    | true, true -> FontStyle.Bold ||| FontStyle.Italic
    | false, false -> FontStyle.Regular
  let name = attr(node, "font", "Calibri")
  new Font(name, float32(attr(node, "size", "12")), style)

let rec loadPart(node:XElement) =
  match node.Name.LocalName with
  | "titled" ->
    let tx = { Text = attr(node, "title", ""); Font = parseFont node}
    let body = loadPart(Seq.head(node.Elements()))
    TitledPart(tx, body)
  | "split" ->
    let orient = parseOrientation node
    let nodes = node.Elements() |> List.ofSeq |> List.map loadPart
    SplitPart(orient, nodes)
  | "text" ->
    TextPart({Text = node.Value; Font = parseFont node})
  | "image" ->
    ImagePart(attr(node, "filename", ""))
  | name -> failwith("Unknown node: " + name)

open System.Windows.Forms

let doc = loadPart(XDocument.Load(@"DocumentRendering\document.xml").Root)
// let bounds = { Left = 0.0f; Top = 0.0f; Width = 520.0f; Height = 630.0f }
// let parts = documentToScreen(doc, bounds)
// let img = drawImage (570, 680) 25.0f (drawElements parts)
// let main = new Form(Text = "Document", BackgroundImage = img, ClientSize = Size(570, 680))
// main.Show()

let rec mapDocument f docPart =
  let processed =
    match docPart with
    | TitledPart(tx, content) ->
        TitledPart(tx, mapDocument f content)
    | SplitPart(orientation, parts) ->
        let mappedParts = parts |> List.map (mapDocument f)
        SplitPart(orientation, mappedParts)
    | _ -> docPart
  f(processed)
let isText(part) =
  match part with | TextPart(_) -> true | _ -> false
let shrinkDocument part =
  match part with
  | SplitPart(_, parts) when List.forall isText parts ->
      let res =
        List.fold (fun st (TextPart(tx)) ->
          { Text = st.Text + " " + tx.Text
            Font = tx.Font } )
          { Text = ""; Font = null } parts
      TextPart(res)
  | part -> part
let shrinkedDoc = doc |> mapDocument shrinkDocument
let bounds = { Left = 0.0f; Top = 0.0f; Width = 520.0f; Height = 630.0f }
let parts = documentToScreen(shrinkedDoc, bounds)
let img = drawImage (570, 680) 25.0f (drawElements parts)
let main = new Form(Text = "Document", BackgroundImage = img, ClientSize = Size(570, 680))
// main.Show()

let rec aggregateDocument f state docPart =
  let state = f state docPart
  match docPart with
  | TitledPart(_, part) ->
      aggregateDocument f state part
  | SplitPart(_, parts) ->
      List.fold (aggregateDocument f) state parts
  | _ -> state

let totalWords =
  aggregateDocument (fun count part ->
    match part with
    | TextPart (tx) | TitledPart (tx, _) ->
        count + tx.Text.Split(' ').Length
    | _ -> count) 0 doc